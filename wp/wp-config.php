<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、言語、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'plushp01_kfujino');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'plushp01_kfujino');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'c4giDA3xcemAwNj');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql21a.xserver.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YtAj/[[OHnQ~v=k?AEATOe|hwx*+KX,I2P?|{btzk*9+Q[p1|/51B>Ln:S)BK2hy');
define('SECURE_AUTH_KEY',  'hBTdKW2l.|e*/:G},xA;LyAMO40)u#x!U;GxVrmJh0A,x[3_Gg -9T7kgIL(m.fK');
define('LOGGED_IN_KEY',    '4v)&a}hSu+**JUE|/cCeh m!-zqGd;iM+~)o [9D%3RW1(cGL,=.FN]41u_}YHv7');
define('NONCE_KEY',        '-vAo^.}-V8N+_BYbd>oT;f>YJSu;r]|(Vu4riv7u 2 HO|9:2JC[+_>$~-@>8+S-');
define('AUTH_SALT',        '{F]cQ-1xu+|4x+(+P5S<ncHl}}:zp&_/xT38{Sc[Y-$Ax*7;4UlI44-5wQf{7Q(%');
define('SECURE_AUTH_SALT', '+,.$a~4/A-Z#a7G^-#c,`|^q(R$t!Hh&P[/IKe;5*c?Ut`Z,6[&&[%ddwM2vI{dC');
define('LOGGED_IN_SALT',   'so!p2L~eG)dv*G-5..?u~Y7USHO `,2nzlNb4ZFjYt/{9p{l`0sD(y(@t,hjhM2L');
define('NONCE_SALT',       'krlwc+Ghr&{7m0BjTspz2W_|l$)VrRpNBD(uk7+#]|mmF+h`nOLd}nh.h^G8|uWA');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_umedalaw_';

/**
 * ローカル言語 - このパッケージでは初期値として 'ja' (日本語 UTF-8) が設定されています。
 *
 * WordPress のローカル言語を設定します。設定した言語に対応する MO ファイルが
 * wp-content/languages にインストールされている必要があります。たとえば de_DE.mo を
 * wp-content/languages にインストールし WPLANG を 'de_DE' に設定すると、ドイツ語がサポートされます。
 */
define('WPLANG', 'ja');

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
